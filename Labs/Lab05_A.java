package Labo5A;

class Students{
	String name;
	int stNumber;
	int age;
	String department;
	int semMarks;
}

public class Lab05_A {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Students st = new Students();
		
		st.name = "Ramesh";
		st.age = 25;
		st.department = "Computer Science";
		st.stNumber = 253110101;
		st.semMarks = 67;
		
		System.out.println("Name: "+st.name+"\nStudent Number: "+st.stNumber+"\nAge: "+st.age);
		System.out.println("Department: "+st.department+"\nSemester Mark: "+st.semMarks);
	}

}
