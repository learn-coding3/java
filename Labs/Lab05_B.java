package Labo5A;
//package Labo5A;

public class Lab05_B {

		String studname, dept;
        int studid, test, assignment, project, tMarks;
        // Constructor initialized
        public Lab05_B (String studname, int studid, String dept, int test, int ass, int pro, int tot)
        {
            this.studname = studname;
            this.studid = studid;
            this.dept = dept;
            this.test = test;
            this.assignment = ass;
            this.project = pro;
            this.tMarks = tot;
        }
        public static void main(String[] args) {
            //Object created
            Lab05_B John = new Lab05_B("John", 8886, "Computer Science",8,17,29,54);
            Lab05_B Steven = new Lab05_B("Steven", 888611, "Computer Science",18,27,19,64);
            Lab05_B Tessy = new Lab05_B("Tessy", 18886, "Computer Science",7,11,19,37);
            Lab05_B Jane = new Lab05_B("Jane", 811886, "Computer Science",18,27,21,66);
            Lab05_B Saima = new Lab05_B("Saima", 22886, "Computer Science",16,27,22,65);
            Lab05_B Pius= new Lab05_B("Pius", 223886, "Computer Science",12,19,32,63);
            
            System.out.println("\n  NAME"+"  "+"ID"+"         "+"DEPT"+"	        "+"TEST"+"   "+"ASSIGN"+" "+"PROJECT"+" "+"TOTALMARK");
            System.out.println("Student's Record #1:"+John.studname+"   "+John.studid+"     "+John.dept+" "+John.test+"       "+John.assignment+" \t"+John.project+" "+John.tMarks);
            System.out.println("Student's Record #2:"+Steven.studname+" "+Steven.studid+"   "+Steven.dept+" "+Steven.test+"      "+Steven.assignment+" \t"+Steven.project+" "+Steven.tMarks);
            System.out.println("Student's Record #3:"+Tessy.studname+" "+Tessy.studid+"     "+Tessy.dept+" "+Tessy.test+"       "+Tessy.assignment+" \t"+Tessy.project+" "+Tessy.tMarks);
            System.out.println("Student's Record #4:"+Jane.studname+" "+Jane.studid+"     "+Jane.dept+" "+Jane.test+"      "+Jane.assignment+" \t"+Jane.project+" "+Jane.tMarks);
            System.out.println("Student's Record #5:"+Saima.studname+" "+Saima.studid+"     "+Saima.dept+" "+Saima.test+"      "+Saima.assignment+" \t"+Saima.project+" "+Saima.tMarks);
            System.out.println("Student's Record #6:"+Pius.studname+" "+Pius.studid+"     "+Pius.dept+" "+Pius.test+"      "+Pius.assignment+" \t"+Pius.project+" "+Pius.tMarks);
            
            
	}
}