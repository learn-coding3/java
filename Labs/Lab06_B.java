package Labo5A;

//import java.util.Scanner;
class Animal1 {
	// field and method of the parent class
	 	private String name;
		public void eat() {
			System.out.println("I can eat very well");
		}
		
		public void Setname(String name) {
			this.name = name;
		}
		public String GetName() {
			return this.name;
		}
	}
	// inherit from Animal
	class Dog extends Animal1 {
	// new method in subclass
		public void display() {
			System.out.println("My surname is " + GetName());
		}
	}
	public class Lab06_B {
		public static void main(String[] args) {
			// create an object of the subclass
			Dog labrador = new Dog();
			// access field of superclass
			labrador.Setname("Rohuda");
			labrador.display();
			// call method of superclass
			// using object of subclass
			labrador.eat();
		}
	}
