//package Labo5A;

import java.util.Scanner;

class Employee1 { 

	// field and method of the parent class

	float basic_sal=40000; 

	float housing_all=20000;

	float transport_all=7000;

	} 

	// inherit from Employee1

	class programmer extends Employee1 { 
		
	String FirstName = "Steven",LastName = "Job";

	float bonus=12000; 

	float overtime=9100;

	float prog_total_sal;

	}
	
	// Admin
	class Admin extends Employee1 { 
		
		String FirstName = "Taka",LastName = "Duve";

		float bonus=10000; 

		float overtime=900;

		float ad_total_sal;

		}
	// Libralian
	class Libralian extends Employee1 { 
		
		String FirstName = "You",LastName ="Game";

		float bonus=90000; 

		float overtime=1900;

		float lib_total_sal;

		}

	public class Lab05_C {

	public static void main(String args[]) { 
		
	    Admin admin = new Admin();
	    Libralian lib = new Libralian();
	    programmer prog=new programmer(); 

	        Scanner kb = new Scanner(System.in);
	
	
    System.out.println("EMPLOYEE #1: ");

	        System.out.println("Enter a First Name: \n"+prog.FirstName);

	        System.out.println("Enter a Last Name: \n"+prog.LastName);
	      
	        
	prog.prog_total_sal = prog.basic_sal + prog.housing_all + prog.transport_all + prog.bonus + prog.overtime;

	System.out.println("Programmer’s Firstname and LastName are:"+prog.FirstName +" "+prog.LastName); 

	System.out.println("Programmer’s Basic salary is:"+prog.basic_sal); 

	System.out.println("Programmers Housing Allowance is:"+prog.housing_all); 

	System.out.println("Programmers Transport Allowance is:"+prog.transport_all); 

	System.out.println("Bonus of Programmer is:"+prog.bonus); 

	System.out.println("overtime of Programmer is:"+prog.overtime); 

	System.out.println("TOTAL SALARY OF PROGRAMMER IS:"+prog.prog_total_sal); 

	System.out.println("");
	
	
	

    System.out.println("EMPLOYEE #2: ");

	       System.out.println("Enter a First Name: \n"+admin.FirstName);

	        System.out.println("Enter a Last Name: \n"+admin.LastName);
	        
	        
	admin.ad_total_sal = admin.basic_sal + admin.housing_all + admin.transport_all + admin.bonus + admin.overtime;

	System.out.println("Programmer’s Firstname and LastName are:"+admin.FirstName +" "+admin.LastName); 

	System.out.println("Admin’s Basic salary is:"+admin.basic_sal); 

	System.out.println("Admin's Housing Allowance is:"+admin.housing_all); 

	System.out.println("Admin's Transport Allowance is:"+admin.transport_all); 

	System.out.println("Bonus of Admin is:"+admin.bonus); 

	System.out.println("overtime of Admin is:"+admin.overtime); 

	System.out.println("TOTAL SALARY OF ADMIN STAFF IS:"+admin.ad_total_sal); 

	System.out.println("");
	
	
	        
	System.out.println("EMPLOYEE #3: ");

	        System.out.println("Enter a First Name: \n"+lib.FirstName);

	        System.out.println("Enter a Last Name: \n"+lib.LastName);
	        
	        
	lib.lib_total_sal = lib.basic_sal + lib.housing_all + lib.transport_all + lib.bonus + lib.overtime;

	System.out.println("Librarian’s Firstname and LastName are:"+lib.FirstName +" "+lib.LastName); 

	System.out.println("Librarian’s Basic salary is:"+lib.basic_sal); 

	System.out.println("Librarian's Housing Allowance is:"+lib.housing_all); 

	System.out.println("Librarian's Transport Allowance is:"+lib.transport_all); 

	System.out.println("Bonus of Librarian is:"+lib.bonus); 

	System.out.println("overtime of Librarian is:"+lib.overtime); 

	System.out.println("TOTAL SALARY OF LIBARY STAFF IS:"+lib.lib_total_sal); 

	System.out.println("");

	}
}

