package Labo5A;

import java.util.Scanner;
	
public class Lab06_C {
	public static void main(String[] args) {
		
		int num;
		Scanner kb = new Scanner(System.in);
		System.out.println("Enter a number");
		num = kb.nextInt();
		
		findCube(num);
			
	}
	public static void findCube(int number) {
		int cube = (number*number*number);
		if(number < 0) {
			System.out.print("Number can not be less than zero !!!");
		}
		else {
			System.out.print("The Cube is: "+cube);
		}
	}
}

